import Vue from "vue";

const Bus = new Vue();

const Util = {
	raw(observer) {
		return JSON.parse(JSON.stringify(observer));
	}
};

const Filters = {
	currency(value) {
		if (!value) return "";
		value = value.toString();
		return `$${value}`;
	}
};

export { Util, Bus, Filters };
