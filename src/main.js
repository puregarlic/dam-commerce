import Vue from "vue";
import App from "./App.vue";

/* eslint-disable import/no-unassigned-import */
import "semantic-ui-css/semantic.min.css";
import "semantic-ui-css/semantic.min";

/* eslint-disable no-new */
new Vue({
	el: "#app",
	template: "<App/>",
	components: { App }
});
